package model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VanityNumberTest {

	@Test
	void testFind() {

		HashMap<String, ArrayList<String>> numberLookupMap = new HashMap<>();
		numberLookupMap.put("123", new ArrayList<String>(Arrays.asList("bee", "boo")));
		numberLookupMap.put("456", new ArrayList<String>(Arrays.asList("mee", "moo")));
		numberLookupMap.put("789", new ArrayList<String>(Arrays.asList("pee", "poo")));

		VanityNumber vn = VanityNumber.getInstance(Config.Langs.GERMAN, true, true, true, numberLookupMap);
		MatchList matches = vn.find("123456");
		assertEquals(9, matches.size());
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("bee", "moo")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("bee", "mee")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("boo", "moo")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("boo", "mee")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("bee", "4", "5", "6")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("boo", "4", "5", "6")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("1", "2", "3", "mee")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("1", "2", "3", "moo")))));
		assertTrue(matches.contains(new Match(new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5", "6")))));

	}

}
