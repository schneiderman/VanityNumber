package model;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.FileTransformer;

class FileTransformerTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@SuppressWarnings("unchecked")
	@Test
	void testGetVariations() {

		HashMap<String, String> replacementList = new HashMap<String, String>() {
			{
				put("[Ee]in", "1");
				put("ü", "ue");
			}
		};
		String word = "Einsteinvereinrübe";

		FileTransformer ft = new FileTransformer("");
		try {
			Method getVariationsMethod = FileTransformer.class.getDeclaredMethod("getVariations", String.class, HashMap.class);
			getVariationsMethod.setAccessible(true);
			ArrayList<String> result;
			result = (ArrayList<String>) getVariationsMethod.invoke(ft, word, replacementList);
			assertNotNull(result);
			assertEquals(16, result.size());
			assertTrue(result.contains("Einsteinvereinrübe"));
			assertTrue(result.contains("1steinvereinrübe"));
			assertTrue(result.contains("Einst1vereinrübe"));
			assertTrue(result.contains("1st1vereinrübe"));
			assertTrue(result.contains("Einsteinver1rübe"));
			assertTrue(result.contains("1steinver1rübe"));
			assertTrue(result.contains("Einst1ver1rübe"));
			assertTrue(result.contains("1st1ver1rübe"));
			assertTrue(result.contains("Einsteinvereinruebe"));
			assertTrue(result.contains("1steinvereinruebe"));
			assertTrue(result.contains("Einst1vereinruebe"));
			assertTrue(result.contains("1st1vereinruebe"));
			assertTrue(result.contains("Einsteinver1ruebe"));
			assertTrue(result.contains("1steinver1ruebe"));
			assertTrue(result.contains("Einst1ver1ruebe"));
			assertTrue(result.contains("1st1ver1ruebe"));
			assertFalse(result.contains(""));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@SuppressWarnings("unchecked")
	@Test
	void testGetVariations2() {
		
		HashMap<String, String> replacementList = new HashMap<String, String>() {
			{
				put("[Ee]ins", "1");
				put("[Ee]in", "1");
				put("ü", "ue");
			}
		};
		String word = "Einsaaten";
		
		FileTransformer ft = new FileTransformer("");
		try {
			Method getVariationsMethod = FileTransformer.class.getDeclaredMethod("getVariations", String.class, HashMap.class);
			getVariationsMethod.setAccessible(true);
			ArrayList<String> result;
			result = (ArrayList<String>) getVariationsMethod.invoke(ft, word, replacementList);
			assertNotNull(result);
			assertTrue(result.contains("Einsaaten"));
			assertTrue(result.contains("1saaten"));
			assertTrue(result.contains("1aaten"));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Test
	void testTranslateAndSave() {

		FileTransformer ft = new FileTransformer("");

		try {
			Field field = FileTransformer.class.getDeclaredField("numberLookupMap");
			field.setAccessible(true);
			HashMap<String, ArrayList<String>> numberLookupMap = (HashMap<String, ArrayList<String>>) field.get(ft);
			HashMap<String, String> testPairs = new HashMap<String, String>();
			testPairs.put("foobar", "366227");
			testPairs.put("brått", "27288");
			testPairs.put("Århus", "27487");

			Method translateAndSaveMethod = FileTransformer.class.getDeclaredMethod("translateAndSave", String.class);
			translateAndSaveMethod.setAccessible(true);

			int cnt = 0;
			for (Entry<String, String> entry : testPairs.entrySet()) {
				String word = entry.getKey();
				Object number = entry.getValue();

				try {
					translateAndSaveMethod.invoke(ft, word);
				} catch (Exception e) {
					fail();
				}
				assertEquals(++cnt, numberLookupMap.size());
				assertTrue(numberLookupMap.containsKey(number));
				assertTrue(numberLookupMap.containsValue(Arrays.asList(word)));
				assertEquals(word, numberLookupMap.get(number).get(0));
			}
		} catch (NoSuchFieldException | SecurityException | NoSuchMethodException | IllegalArgumentException
				| IllegalAccessException e2) {
			e2.printStackTrace();
		}
	}

	@Test
	void testTranslateAndSaveException() {

		FileTransformer ft = new FileTransformer("");

		try {
			Method translateAndSaveMethod = FileTransformer.class.getDeclaredMethod("translateAndSave", String.class);
			translateAndSaveMethod.setAccessible(true);
			
			Assertions.assertThrows(Exception.class, () -> {
				translateAndSaveMethod.invoke(ft, "Foo濁Bar");
			});
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
