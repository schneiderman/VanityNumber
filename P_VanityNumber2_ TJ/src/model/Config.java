package model;

import java.io.File;
import java.util.HashMap;

@SuppressWarnings("serial")
public class Config {

	/*
	 * Languages offered
	 */
	public static enum Langs {
		GERMAN, ENGLISH, SPANISH, ITALIAN, FRENCH
	}

	/*
	 * maximum number of letters a word can have to be stored
	 * 
	 * This has a big influence on the size of the generated files. Keep it as low
	 * as possible. German Numbers can be 15 digits max.
	 */
	public final static int maximumLetters = 15;

	/*
	 * minimum number of digits to lookup in lookup map
	 * 
	 * this should be set to either 1 or 2. If set to 1 we also lookup single digits
	 * and if there are representations of those in the lookup map we can end up
	 * having matches with single letters, eg. 234 -> a|d|g, a|d|h, a|e|h, etc on
	 * the other hand we might miss out actual 1-letter words like 'a' in
	 * "drink·a·tea"
	 */
	public final static int minimumDigitsToLookup = 2;

	/**
	 * this is the separator for the matched words for one vanity number
	 */
	public final static char separator = '·';

	/*
	 * folder where we write the lookupMap files. We need to have a OS independent
	 * place to make the exported jar work
	 */
	public final static String outputFolder = System.getProperty("java.io.tmpdir");

	/*
	 * folder where we read the dictionary files. they need to be present as
	 * <lowercase LANG>.txt, eg. en.txt
	 */
	public final static String inputFolder = "/data/";

	/*
	 * lookup map for chars to digits
	 */
	public final static HashMap<Character, Integer> char2digitLookupMap = new HashMap<>();
	static {
		char2digitLookupMap.put('a', 2);
		char2digitLookupMap.put('ã', 2);
		char2digitLookupMap.put('æ', 2);
		char2digitLookupMap.put('ä', 2);
		char2digitLookupMap.put('â', 2);
		char2digitLookupMap.put('à', 2);
		char2digitLookupMap.put('á', 2);
		char2digitLookupMap.put('á', 2);
		char2digitLookupMap.put('å', 2);
		char2digitLookupMap.put('b', 2);
		char2digitLookupMap.put('c', 2);
		char2digitLookupMap.put('ç', 2);
		char2digitLookupMap.put('d', 3);
		char2digitLookupMap.put('e', 3);
		char2digitLookupMap.put('ë', 3);
		char2digitLookupMap.put('è', 3);
		char2digitLookupMap.put('ê', 3);
		char2digitLookupMap.put('é', 3);
		char2digitLookupMap.put('f', 3);
		char2digitLookupMap.put('g', 4);
		char2digitLookupMap.put('h', 4);
		char2digitLookupMap.put('i', 4);
		char2digitLookupMap.put('í', 4);
		char2digitLookupMap.put('ì', 4);
		char2digitLookupMap.put('ï', 4);
		char2digitLookupMap.put('î', 4);
		char2digitLookupMap.put('j', 5);
		char2digitLookupMap.put('k', 5);
		char2digitLookupMap.put('l', 5);
		char2digitLookupMap.put('m', 6);
		char2digitLookupMap.put('n', 6);
		char2digitLookupMap.put('ñ', 6);
		char2digitLookupMap.put('o', 6);
		char2digitLookupMap.put('ø', 6);
		char2digitLookupMap.put('ô', 6);
		char2digitLookupMap.put('ô', 6);
		char2digitLookupMap.put('ó', 6);
		char2digitLookupMap.put('ò', 6);
		char2digitLookupMap.put('ö', 6);
		char2digitLookupMap.put('p', 7);
		char2digitLookupMap.put('q', 7);
		char2digitLookupMap.put('r', 7);
		char2digitLookupMap.put('s', 7);
		char2digitLookupMap.put('ß', 7);
		char2digitLookupMap.put('t', 8);
		char2digitLookupMap.put('u', 8);
		char2digitLookupMap.put('û', 8);
		char2digitLookupMap.put('ü', 8);
		char2digitLookupMap.put('ù', 8);
		char2digitLookupMap.put('ú', 8);
		char2digitLookupMap.put('v', 8);
		char2digitLookupMap.put('w', 9);
		char2digitLookupMap.put('x', 9);
		char2digitLookupMap.put('y', 9);
		char2digitLookupMap.put('z', 9);
		char2digitLookupMap.put('0', 0);
		char2digitLookupMap.put('1', 1);
		char2digitLookupMap.put('2', 2);
		char2digitLookupMap.put('3', 3);
		char2digitLookupMap.put('4', 4);
		char2digitLookupMap.put('5', 5);
		char2digitLookupMap.put('6', 6);
		char2digitLookupMap.put('7', 7);
		char2digitLookupMap.put('8', 8);
		char2digitLookupMap.put('9', 9);
	}

	/*
	 * HashMap to convert "lachte" to "l8e" or "stencil" to "s10cil"
	 * 
	 * do not combine words of different length in one regex as you 
	 * can miss out a combination: eg. "[Ee]ins?" applied on "Einsaat"
	 * will only match "1aat", but not "1saat"
	 * 
	 */

	public static HashMap<Langs, HashMap<String, String>> spellingNumbers = new HashMap<>();
	static {
		spellingNumbers.put(Langs.GERMAN, new HashMap<String, String>() {
			{
				put("[Nn]ull", "0");
				put("[Ee]ins", "1");
				put("[Ee]in", "1");
				put("[Zz]wei", "2");
				put("[Dd]rei", "3");
				put("[Vv]ier", "4");
				put("[Ff][uü]nf", "5");
				put("[Ss]echs", "6");
				put("[Ss]ieben", "7");
				put("[Aa]cht", "8");
				put("[Nn]eun", "9");
				put("[Zz]ehn", "10");
				put("[Ee]lf", "11");
			}
		});
		spellingNumbers.put(Langs.ENGLISH, new HashMap<String, String>() {
			{
				put("[Nn]ull", "0");
				put("[Zz]ero", "0");
				put("[Oo]ne", "1");
				put("[Tt]wo", "2");
				put("[Tt]hree", "3");
				put("[Ff]our", "4");
				put("[Ff]ive", "5");
				put("[Ss]ix", "6");
				put("[Ss]even", "7");
				put("[Ee]ight", "8");
				put("[Nn]ine", "9");
				put("[Tt]en", "10");
				put("[Ee]leven", "11");
			}
		});
		spellingNumbers.put(Langs.SPANISH, new HashMap<String, String>() {
			{
				put("[Cc]ero", "0");
				put("[Nn]ulo", "0");
				put("[Uu]no", "1");
				put("[Uu]na", "1");
				put("[Dd]os", "2");
				put("[Tt]res", "3");
				put("[Cc]uadro", "4");
				put("[Cc]inco", "5");
				put("[Ss]eis", "6");
				put("[Ss]iete", "7");
				put("[Oo]cho", "8");
				put("[Nn]ueve", "9");
				put("[Dd]iez", "10");
				put("[Oo]nce", "11");
				put("[Dd]oce", "12");
				put("[Tt]rece", "13");
				put("[Cc]atorce", "14");
				put("[Qq]uince", "15");
			}
		});
		spellingNumbers.put(Langs.ITALIAN, new HashMap<String, String>() {
			{
				put("[Zz]ero", "0");
				put("[Uu]no", "1");
				put("[Dd]ue", "2");
				put("[Tt]re", "3");
				put("[Qq]uattro", "4");
				put("[Cc]inque", "5");
				put("[Ss]ei", "6");
				put("[Ss]ette", "7");
				put("[Oo]tto", "8");
				put("[Nn]ove", "9");
				put("[Dd]ieci", "10");
				put("[Vv]enti", "20");
				put("[Tt]renta", "30");
				put("[Cc]ento", "100");
				put("[Mm]ille", "1000");
			}
		});
		spellingNumbers.put(Langs.FRENCH, new HashMap<String, String>() {
			{
				put("[Zz][ée]ro", "0");
				put("[Nn]ul", "0");
				put("[Nn]ulle", "0");
				put("[Uu]n", "1");
				put("[Dd]eux", "2");
				put("[Tt]rois", "3");
				put("[Qq]uatre", "4");
				put("[Cc]inq", "5");
				put("[Ss]ix", "6");
				put("[Ss]ept", "7");
				put("[Hh]uit", "8");
				put("[Nn]euf", "9");
				put("[Dd]ix", "10");
				put("[Oo]nze", "11");
				put("[Dd]ouze", "12");
				put("[Tt]reize", "13");
				put("[Qq]uatorze", "14");
				put("[Qq]uinze", "15");
				put("[Ss]eize", "16");
				put("[Vv]ingt", "20");
				put("[Tt]rente", "30");
				put("[Cc]ent", "100");
				put("[Mm]ille", "1000");

			}
		});
	}

	/*
	 * to convert "Häme" to "Haeme" or "übel" to "uebel"
	 * 
	 * These go in as alternatives, eg. "Häme", will match 4263 and 42363
	 * 
	 */
	public static HashMap<Langs, HashMap<String, String>> umlaute = new HashMap<>();
	static {
		umlaute.put(Langs.GERMAN, new HashMap<String, String>() {
			{
				put("ä", "ae");
				put("æ", "ae");
				put("ü", "ue");
				put("ö", "oe");
				put("ß", "ss");
				put("Ä", "Ae");
				put("Æ", "Ae");
				put("Ü", "Ue");
				put("Ö", "Oe");
			}
		});
		umlaute.put(Langs.SPANISH, new HashMap<String, String>() {
			{
				put("ñ", "ny");
			}
		});

	}

}
