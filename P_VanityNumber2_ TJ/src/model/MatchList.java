package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MatchList implements Iterable<Match> {

	private static Logger log = LogManager.getLogger(MatchList.class);

	private List<Match> matches = new ArrayList<>();

	public List<Match> getMatches() {
		return matches;
	}

	public int size() {
		return matches.size();
	}

	public void addWord(String word) {
		addWords(new ArrayList<String>(Arrays.asList(word)));
	}
	
	public Stream<Match> stream() {
		return matches.stream();
	}

	public void addWords(ArrayList<String> words) {
		if (this.matches.size() == 0) {
			for (String word : words) {
				this.matches.add(new Match(word));
			}
		} else {
			List<Match> tempMatches = new ArrayList<>();
			for (Match match : this.matches) {
				for (String word : words) {
					Match m = new Match(match.getWords());
					m.add(word);
					tempMatches.add(m);
				}
			}
			this.matches = tempMatches;
		}
	}

	public void addMatchList(MatchList matchList) {
		matches.addAll(matchList.getMatches());
	}
	
	public void generateStatistics() {
		matches.stream().forEach(Match::generateStatistics);
	}

	public void clear() {
		matches.clear();
	}

	public ArrayList<String> getAsList() {
		return (ArrayList<String>) matches.stream().map(m -> m.toString()).collect(Collectors.toList());
	}
	
	public boolean contains(Match m) {
		return matches.contains(m);
	}

	@Override
	public Iterator<Match> iterator() {
		
		return new Iterator<Match>() {

			int cursor = 0;
			
			@Override
			public boolean hasNext() {
	            if (cursor < MatchList.this.matches.size()) {
	                return true;
	            } else {
	                return false;
	            }
			}

			@Override
			public Match next() {
				
				if(this.hasNext()) {
	                Match m = MatchList.this.matches.get(cursor);
	                cursor++;
	                return m;
	            }
	            throw new NoSuchElementException();
			}
		};
	}

}
