package model;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Match {
	
	private static Logger log = LogManager.getLogger(Match.class);
	
	private ArrayList<String> words = new ArrayList<>();
	
	private double letterCount = 0;
	public double getLetterCount() {
		return letterCount;
	}

	private double wordCount = 0;
	public double getWordCount() {
		return wordCount;
	}

	private double digitCount = 0;
	public double getDigitCount() {
		return digitCount;
	}

	private double digitCountQuality = 0.0;
	public double getDigitCountQuality() {
		return digitCountQuality;
	}

	private double wordCountQuality = 0.0;
	public double getWordCountQuality() {
		return wordCountQuality;
	}
	
	private double overallQuality = 0.0;
	public double getOverallQuality() {
		return overallQuality;
	}
	
	public ArrayList<String> getWords() {
		return words;
	}
	public Match(String word) {
		addAll(new ArrayList<>(Arrays.asList(word)));
	}
	public Match(ArrayList<String> words) {
		addAll(words);
	}
	
	public void add(String word) {
		addAll(new ArrayList<>(Arrays.asList(word)));
	}
	
	public void addAll(ArrayList<String> words) {
		this.words.addAll(words);
	}
	
	/*
	 * Generate all the counters and qualities
	 */
	public void generateStatistics() {
		letterCount = String.join("", words).length();
		wordCount = words.size();
		wordCountQuality = ((letterCount-wordCount)/letterCount)*(letterCount/(letterCount-1));
		digitCount = 0;
		for (String word : words) {
			if(word.chars().allMatch( Character::isDigit )) {
				digitCount++;
			}
		}
		digitCountQuality = (wordCount-digitCount)/wordCount;
		// TODO: does it make sense to weigh both equally?
		overallQuality = (digitCountQuality+wordCountQuality)/2;
	}

	@Override
	public String toString() {
		return String.join(String.valueOf(Config.separator), words);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((words == null) ? 0 : words.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Match other = (Match) obj;
		if (words == null) {
			if (other.words != null)
				return false;
		} else if (!words.equals(other.words))
			return false;
		return true;
	}
	
}
