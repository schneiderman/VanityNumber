package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

public class FilesGeneratorService extends Service<Boolean> {

	private static Logger log = LogManager.getLogger(FilesGeneratorService.class);

	/*
	 * total number of files to be generated
	 * number of languages * 1/i replacement or not * 0/o replacement or not * spelling or not
	 * this is needed for the updateProgress() method 
	 */
	private static final int numberOfFilesToBeGenerated = Config.Langs.values().length * 2 * 2 * 2;

	@Override
	protected Task<Boolean> createTask() {
		Task<Boolean> task = new Task<Boolean>() {
			@Override
			protected Boolean call() throws Exception {

				int numberOfFilesGenerated = 0;

				updateProgress(numberOfFilesGenerated, FilesGeneratorService.numberOfFilesToBeGenerated);

				for (Config.Langs lang : Config.Langs.values()) {

					boolean doSpellingNumbers[] = { false, true };
					for (boolean spell : doSpellingNumbers) {

						boolean read0asO[] = { false, true };
						for (boolean o : read0asO) {

							boolean read1asI[] = { false, true };
							for (boolean i : read1asI) {

								String outputFileName = VanityNumber.getOuputFileName(lang, spell, o, i);
								
								if (!new File(outputFileName).exists()) {
									
									updateMessage("Generating file: " + outputFileName + "\n");
									
									// creating FileTransformer object with settings
									FileTransformer ft = new FileTransformer(VanityNumber.getInputFileName(lang));
									if (Config.umlaute.containsKey(lang)) {
										ft.setUmlaute(Config.umlaute.get(lang));
									}
									if (spell) {
										ft.addSpellingNumbers(Config.spellingNumbers.get(lang));
									}
									ft.setRead0asO(o);
									ft.setRead1asI(i);

									HashMap<String, ArrayList<String>> numberLookupMap = ft.generate();
									log.info("done");

									log.info("- Writing to file ... ");
									BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFileName));
									for (HashMap.Entry<String, ArrayList<String>> entry : numberLookupMap.entrySet()) {
										String line = entry.getKey() + ":";
										line += String.join(",", entry.getValue());
										outputWriter.write(line);
										outputWriter.newLine();
									}
									outputWriter.close();

									log.info("done");
								}
								updateProgress(++numberOfFilesGenerated,
										FilesGeneratorService.numberOfFilesToBeGenerated);
							}

						}

					}
				}
				updateMessage("\nAll files generated!");
				return true;
			}
		};
		return task;
	}

}
