package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

public class VanityNumber {

	private static Logger log = LogManager.getLogger(VanityNumber.class);

	public static String getOuputFileName(Config.Langs language, boolean doSpellingNumbers, boolean read0asO,
			boolean read1asI) {
		String fileName = Config.outputFolder + "vanity-" + language.toString().toLowerCase();
		fileName += (doSpellingNumbers ? "-spelling" : "");
		fileName += (read0asO ? "-0asO" : "");
		fileName += (read1asI ? "-1asI" : "");
		fileName += ".dic";
		return fileName;
	}

	public static String getInputFileName(Config.Langs language) {
		String fileName = Config.inputFolder + language.toString().toLowerCase() + ".txt";
		return fileName;
	}

	/**
	 * if set, we match "Seg11r8er" for "Segelfrachter"
	 */
	private static boolean acceptSpellingNumbers = false;

	/**
	 * if set, 8083 matches "t0te"
	 */
	private static boolean read0asO = false;

	/**
	 * if set, 81373 matches "t1ere"
	 */
	private static boolean read1asI = false;

	private static Config.Langs language;

	private static VanityNumber instance = null;

	/*
	 * make sure we always have only one instance
	 */
	public static VanityNumber getInstance(Config.Langs lang, boolean acceptSpellingNumbers, boolean read0asO,
			boolean read1asI, HashMap<String, ArrayList<String>> numberLookupMap) {
		if (instance == null || !VanityNumber.language.equals(lang)
				|| !VanityNumber.acceptSpellingNumbers == acceptSpellingNumbers || !VanityNumber.read0asO == read0asO
				|| !VanityNumber.read1asI == read1asI) {
			VanityNumber.language = lang;
			VanityNumber.acceptSpellingNumbers = acceptSpellingNumbers;
			VanityNumber.read0asO = read0asO;
			VanityNumber.read1asI = read1asI;
			instance = new VanityNumber(numberLookupMap);
		}
		return instance;
	}

	/**
	 * the lookup map for numbers. Every number has one or more matching words eg.
	 * 3647:dogs,emir
	 */
	private HashMap<String, ArrayList<String>> numberLookupMap = new HashMap<>();

	/**
	 * list of vanity-numbers
	 */
	private MatchList matches = new MatchList();

	/**
	 * maximum number of digits allowed in vanity numbers
	 */
	private int maxNumberOfDigits = -1;

	public void setMaxNumberOfDigits(int maxNumberOfDigits) {
		this.maxNumberOfDigits = maxNumberOfDigits;
	}

	/**
	 * maximum number of words per vanity number
	 */
	private int maxNumberOfWords = -1;

	public void setMaxNumberofWords(int maxNumberOfWords) {
		this.maxNumberOfWords = maxNumberOfWords;
	}

	/**
	 * @param numberLookupMap
	 */
	private VanityNumber(HashMap<String, ArrayList<String>> numberLookupMap) {

		log.info("settings: spell:" + acceptSpellingNumbers + ", read0asO:" + read0asO + ", read1asI:" + read1asI
				+ ", words:" + maxNumberOfWords + ", digits:" + maxNumberOfDigits);

		// get lookup-HashMap
		if (numberLookupMap == null) {
			loadNumberLookupMap();
		} else {
			this.numberLookupMap = numberLookupMap;
		}

	}

	private void loadNumberLookupMap() {
		long start = System.currentTimeMillis();
		String inputFileName = VanityNumber.getOuputFileName(language, acceptSpellingNumbers, read0asO, read1asI);

		log.info("Loading " + inputFileName + " ... ");
		BufferedReader inputFile = null;
		try {
			inputFile = new BufferedReader(new FileReader(new File(inputFileName)));
			String line;
			while ((line = inputFile.readLine()) != null) {
				String parts[] = line.split(":");
				String words[] = parts[1].split(",");
				numberLookupMap.put(parts[0], new ArrayList<>(Arrays.asList(words)));
			}
			inputFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.info("... done after " + (System.currentTimeMillis() - start) / 1000 + "s");
	}

	public MatchList find(String number) {

		number = number.replaceAll("\\D", "");

		log.info("trying to find matches for " + number + "...");

		this.maxNumberOfWords = this.maxNumberOfWords == -1 ? number.length() : this.maxNumberOfWords;
		this.maxNumberOfDigits = this.maxNumberOfDigits == -1 ? number.length() : this.maxNumberOfDigits;

		// reset matches
		matches.clear();

		// do the actual lookup
		parse(number, new MatchList());
		
		matches.generateStatistics();

		log.info("... finished");
		return matches;

	}

	private void parse(String number, MatchList preliminaryMatches) {

		log.debug("function called with number; " + number + ", preliminaryMatches: " + preliminaryMatches.getAsList());

		for (int i = 0; i < number.length(); i++) {

			log.debug(i + 1 + ". loop, preliminaryMatches: " + preliminaryMatches.getAsList());

			// split number in 2 parts
			String part1 = number.substring(0, number.length() - i);
			String part2 = number.substring(number.length() - i, number.length());
			log.debug("- current number: [" + part1 + "]");

			ArrayList<String> foundWords = new ArrayList<>();

			// try to find number in HashMap
			if (part1.length() >= Config.minimumDigitsToLookup) {
				if (numberLookupMap.containsKey(part1)) {
					foundWords.addAll(numberLookupMap.get(part1));
					log.debug("- matched words in map: " + numberLookupMap.get(part1));

				}
			}
			// add bare digit if part1 is just one digit
			if (part1.length() == 1) {
				log.debug("- matched bare number: " + part1);
				foundWords.add(part1);
			}

			log.debug("- foundWords: " + foundWords);

			if (foundWords.size() > 0) {

				MatchList currentMatches = new MatchList();
				currentMatches.addMatchList(preliminaryMatches);
				
				currentMatches.addWords(foundWords);

				if (currentMatches.size() > 0) {
					if (part2.length() > 0) {
						// there's a word left, let's find matches
						parse(part2, currentMatches);
					} else if (part2.length() == 0) {
						// nothing left, we are done
						this.matches.addMatchList(currentMatches);
					}
				}
			}
		}
		preliminaryMatches.clear();
	}
}
