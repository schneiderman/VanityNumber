package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileTransformer {

	private static Logger log = LogManager.getLogger(FileTransformer.class);
	
	private String fileName;

	/**
	 * if set, we spell ä also like ae, ü also like ue, etc
	 */
	private HashMap<String, String> umlaute = new HashMap<>();
	public void setUmlaute(HashMap<String, String> umlaute) {
		this.umlaute = umlaute;
	}

	/**
	 * Optional: contains word representations of numbers, eg. 8->acht, 11->elf
	 */
	private HashMap<String, String> spellingNumbers;
	public void addSpellingNumbers(HashMap<String, String> spellingNumbers) {
		this.spellingNumbers = spellingNumbers;
	}

	/**
	 * if set, we spell "0" as "o", eg. "8083" matches "tote" as in "t0te"
	 */
	private boolean read0asO = false;
	public void setRead0asO(boolean read0asO) {
		this.read0asO = read0asO;
	}

	/**
	 * if set, we spell "1" as "i", eg. "81373" matches "Tiere" as in "T1ere"
	 */
	private boolean read1asI = false;
	public void setRead1asI(boolean read1asI) {
		this.read1asI = read1asI;
	}

	/**
	 * The generated HashMap to look up numbers, it will contain one number and its
	 * matching words per line
	 */
	private HashMap<String, ArrayList<String>> numberLookupMap = new HashMap<>();

	public FileTransformer(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Run through all lines in a file with 1 word per line, 
	 * find variation for the word then translate the words
	 * to its number and save
	 * 
	 * @return
	 */
	public HashMap<String, ArrayList<String>> generate() {

		HashMap<String, String> replacementList = new HashMap<>();

		if (read0asO) {
			replacementList.put("[Oo]", "0");
		}

		if (read1asI) {
			replacementList.put("[Ii]", "1");
		}

		if (umlaute != null) {
			replacementList.putAll(umlaute);
		}

		if (spellingNumbers != null) {
			replacementList.putAll(spellingNumbers);
		}

		BufferedReader br;
		try {
			log.info("reading file " + fileName);
			InputStream stream = this.getClass().getResourceAsStream(fileName);
			
			br = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
			
			String line;
			while ((line = br.readLine()) != null) {
				if (line.matches("^#.*")) {
					// ignore lines which start with #
					continue;
				}
				if (line.contains(" ")) {
					// ignore lines with whitespaces
					continue;
				}
				line = line.trim();
				line = line.replace("-", "");

				ArrayList<String> words = getVariations(line, replacementList);

//				for (String word : words) {
//					try {
//						translateAndSave(word);
//					} catch (Exception e) {
//						e.printStackTrace();
//						System.exit(1);
//					}
//				}
				// this is slightly faster than the above
				words.stream().forEach(word->{
					try {
						translateAndSave(word);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return numberLookupMap;
	}

	/**
	 * Take a word and translate it to a number, by looking up each char in a
	 * HashMap. Then save it to another HashMap in the form: 7378:Peru,Pest,Rest
	 * 
	 * @param word
	 * @throws Exception 
	 */
	private void translateAndSave(String word) throws Exception {
		String number = "";
		if (word.length() > Config.maximumLetters)
			return;
		char[] chars = word.toCharArray();
		for (char c : chars) {
			if (!Config.char2digitLookupMap.containsKey(Character.toLowerCase(c))) {
				throw new Exception("missing char: <" + c + "> in word: " + word);
			}
			number += String.valueOf(Config.char2digitLookupMap.get(Character.toLowerCase(c)));
		}
		if (numberLookupMap.containsKey(number)) {
			numberLookupMap.get(number).add(word);
		} else {
			numberLookupMap.put(number, new ArrayList<String>(Arrays.asList(word)));
		}
	}

	private ArrayList<String> getVariations(String word, HashMap<String, String> replacementList) {
		ArrayList<String> foundWords = new ArrayList<>(Arrays.asList(word));

		for (Map.Entry<String, String> e : replacementList.entrySet()) {
			String needle = e.getKey();
			String replacement = e.getValue();
			ArrayList<String> wordsToBeProcessed = new ArrayList<>(foundWords);
			for (String wordToBeProcessed : wordsToBeProcessed) {
				ArrayList<String> variationsForOneNeedle = getVariationsForOneNeedle(wordToBeProcessed, needle,
						replacement);
				// Hack: we need to remove the first element to avoid duplicates
				variationsForOneNeedle.remove(0);
				foundWords.addAll(variationsForOneNeedle);
			}
		}
		return foundWords;
	}

	private ArrayList<String> getVariationsForOneNeedle(String word, String needle, String replacement) {

		ArrayList<String> words = new ArrayList<>();

		Pattern p = Pattern.compile(needle);
		Matcher m = p.matcher(word);

		if (m.find()) {
			String part1 = word.substring(0, m.end());
			String part1replaced = part1.replaceFirst(needle, replacement);
			String part2 = word.substring(m.end(), word.length());
			ArrayList<String> variations = getVariationsForOneNeedle(part2, needle, replacement);
			for (String variation : variations) {
				words.add(part1 + variation);
				words.add(part1replaced + variation);
			}
		} else {
			words.add(word);
		}

		return words;

	}

}
