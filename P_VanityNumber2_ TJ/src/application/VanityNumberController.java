package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.StageStyle;
import model.Config;
import model.Config.Langs;
import model.FilesGeneratorService;
import model.Match;
import model.VanityNumber;

public class VanityNumberController implements Initializable {

	@FXML
	TextField numberTextField;

	@FXML
	ListView<Match> outputListView;
	ObservableList<Match> resultList;

	@FXML
	Config.Langs language;

	@FXML
	CheckBox read0asO;
	@FXML
	CheckBox read1asI;

	@FXML
	CheckBox acceptSpellingNumbers;
	@FXML
	ChoiceBox<Object> maxNumberOfDigits;
	@FXML
	ChoiceBox<Object> maxNumberOfWords;

	@FXML
	FlowPane languagePane;

	@FXML
	public void searchAction(ActionEvent event) {

		resultList.clear();

		VanityNumber vn = VanityNumber.getInstance(language, acceptSpellingNumbers.isSelected(), read0asO.isSelected(),
				read1asI.isSelected(), null);

		vn.setMaxNumberOfDigits(maxNumberOfDigits.getSelectionModel().getSelectedIndex());
		vn.setMaxNumberofWords(maxNumberOfWords.getSelectionModel().getSelectedIndex());


		ArrayList<Match> results = (ArrayList<Match>) vn.find(numberTextField.getText()).getMatches();
		ArrayList<Match> filteredResults = (ArrayList<Match>) results.stream()
				.filter(m -> m.getDigitCount() <= maxNumberOfDigits.getSelectionModel().getSelectedIndex())
				.filter(m -> m.getWordCount() <= maxNumberOfWords.getSelectionModel().getSelectedIndex())
				.sorted(Comparator.comparing(Match::getOverallQuality).reversed()).collect(Collectors.toList());
		resultList.addAll(filteredResults);

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		outputListView.setStyle("-fx-font: 20px 'Courier New'");
		resultList = FXCollections.observableArrayList();
		outputListView.setItems(resultList);

		maxNumberOfDigits
				.setItems(FXCollections.observableArrayList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));
		maxNumberOfDigits.getSelectionModel().select(4);

		maxNumberOfWords
				.setItems(FXCollections.observableArrayList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));
		maxNumberOfWords.getSelectionModel().select(4);

		// set radio buttons for each language
		ToggleGroup languageToggleGroup = new ToggleGroup();
		for (Langs language : Config.Langs.values()) {
			RadioButton radioButton = new RadioButton(language.toString().toLowerCase());
			radioButton.setId(language.toString());
			radioButton.setToggleGroup(languageToggleGroup);
			languagePane.getChildren().add(radioButton);
		}

		// listen on language toggle group
		languageToggleGroup.selectedToggleProperty().addListener((a, b, toggle) -> {
			RadioButton radioButton = (RadioButton) toggle;
			language = Config.Langs.valueOf(radioButton.getId());
		});

		// select first radio button
		// this already sets the language value due to the listener above
		languageToggleGroup.getToggles().get(0).selectedProperty().set(true);

		Platform.runLater(() -> {

			Alert alert = new Alert(AlertType.WARNING);
			alert.setHeaderText("Lookup files generation needed!");
			alert.setContentText(
					"In order to speed up the actual search for vanity numbers lookup files need to be generated.\n"
							+ "This may take up to 6 minutes! Please be patient!");
			alert.showAndWait();

			FilesGeneratorService service = new FilesGeneratorService();
			showDialog(service);
			service.restart();

		});

	}

	public void showDialog(FilesGeneratorService service) {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.initStyle(StageStyle.UTILITY);
		alert.setHeaderText("Lookup files generation!");

		// disable OK button until service has finished
		Button button = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
		button.setDisable(true);
		button.setText("Close");
		service.setOnSucceeded(e -> {
			button.setDisable(false);
		});
		// prevent alert window from closing
		alert.getDialogPane().getScene().getWindow().setOnCloseRequest(event -> event.consume());

		// create progressbar and bind on service's progress property
		final ProgressBar progressBar = new ProgressBar();
		progressBar.setProgress(0);
		progressBar.setMaxWidth(Double.MAX_VALUE);
		progressBar.progressProperty().bind(service.progressProperty());

		// create grid pane for Label and ProgressBar and add to content
		GridPane gridPane = new GridPane();
		gridPane.setMaxWidth(Double.MAX_VALUE);
		Label label = new Label(
				"Lookup files are being generated. Please be patient!");
		label.setPadding(new Insets(5d));
		gridPane.add(label, 0, 0);
		gridPane.add(progressBar, 0, 1);
		alert.getDialogPane().setContent(gridPane);

		// create textarea, bind on service's message property and add to expandable
		// content
		TextArea textArea = new TextArea();
		service.messageProperty().addListener((a, b, d) -> {
			textArea.appendText(d);
		});
		textArea.setEditable(false);
		textArea.setWrapText(true);
		alert.getDialogPane().setExpandableContent(textArea);
		alert.getDialogPane().setExpanded(true);
		alert.show();

	}

}
